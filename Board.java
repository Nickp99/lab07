public class Board{
	private Square[][] ticTacToeBoard;
	
	public Board(){
		this.ticTacToeBoard =new Square[3][3];
		
		for(int i=0;i<this.ticTacToeBoard.length;i++){
			
			for(int j=0;j<this.ticTacToeBoard[i].length;j++){
				
				ticTacToeBoard[i][j]= Square.BLANK;
			}
		}
	}
	
	public String toString(){
		String a="";
		for(int i=0;i<this.ticTacToeBoard.length;i++){
			
			for(int j=0;j<this.ticTacToeBoard[i].length;j++){
				
				a+= this.ticTacToeBoard[i][j]+" ";
			}
			a+="\n";
		}
	 return a;
	}
	public boolean placeToken(int row, int col, Square playerToken){
		
		if(row > 3 || row < 1 || col >3 || col <1){
			return false;
		}
		col=col-1;
		row=row-1;
		if(this.ticTacToeBoard[col][row] == Square.BLANK){
			this.ticTacToeBoard[col][row] = playerToken;
			return true;
		}
		else{
			return false;
		}
	}
	public boolean checkIfFull(){
		
		for(int i=0;i < this.ticTacToeBoard.length; i++){
			for(int a=0; a < this.ticTacToeBoard[i].length;a++){
				
				if(this.ticTacToeBoard[i][a]== Square.BLANK){
							
					return false;
					
				}
			}
		}
		return true;
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken){
		int check=0;
		for(int i=0;i < this.ticTacToeBoard.length; i++){
			check =0;
			for(int a=0; a < this.ticTacToeBoard[i].length;a++){
				
				if(this.ticTacToeBoard[i][a]== playerToken){
					check++;
					if(check ==3){
						return true;
					}
				}
			}
		}
		return false;
	} 
	private boolean checkIfWinningVertical(Square playerToken){
		int check=0;
		int i=0;
		while(i<this.ticTacToeBoard.length){
			check =0;
			
			for(int a=0; a < this.ticTacToeBoard[i].length;a++){
				
				if(this.ticTacToeBoard[a][i]== playerToken){
					check++;
					if(check ==3){
						return true;
					}
				}
			}
			i++;
		}
		return false;
	} 
	private boolean checkIfWinningDiagonal(Square playerToken){
		int check=0;
		int backwards=2;
		int i = 0;  //for the for loops
			for(int a=0; a < this.ticTacToeBoard[i].length;a++){
				
				if(this.ticTacToeBoard[a][a]== playerToken){
					check++;
					if(check ==3){
						return true;
					}
				}
			}
		
		check=0; // to rest the check
			for(int b=0; b<this.ticTacToeBoard[i].length;b++){
				if(this.ticTacToeBoard[b][backwards]== playerToken){
					check++;
					if(check ==3){
						return true;
					}
				}
			backwards=backwards-1;	
			}
		return false;
	} 
	
	public boolean checkIfWinning(Square playerToken){
		boolean hor = checkIfWinningHorizontal(playerToken);
		boolean ver = checkIfWinningVertical(playerToken);
		boolean dig = checkIfWinningDiagonal(playerToken);
		if (hor == true || ver == true || dig == true){
			return true;
		}
		else{
			return false;
		}
	}
}
