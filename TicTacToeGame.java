import java.util.Scanner;
public class TicTacToeGame{

	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		System.out.println("Welcome to TikTacToe!");
		Board board = new Board();
		boolean gameOver =false;
		int player=1;
		Square playerToken = Square.X;
		
		
		while(gameOver==false){
			System.out.print(board);
			if (player == 1){
				playerToken = Square.X;
			}
			else{
				playerToken =Square.O;
			}
			System.out.println("Where will you like to place your piece?(first insert the column and then the row):");
			int col=scan.nextInt();
			int row=scan.nextInt();
			boolean noError = board.placeToken(row,col,playerToken);
			while(noError==false){
				System.out.println("Please re-enter the place you want, it was either already taken or out of bound");
				col=scan.nextInt();
				row=scan.nextInt();
				noError = board.placeToken(row,col,playerToken);
			}
			gameOver = board.checkIfWinning(playerToken);
			if(gameOver == true){
					System.out.println("player "+player+" wins the game!!");
					System.out.print(board); // only feels right to see the board when you win
				}
				else{
					gameOver=board.checkIfFull();
					if(gameOver == true){
						System.out.println("It is a tie");
						System.out.print(board); // to show the players it is a draw
					}
					else{
						player++;
						if(player>2){
							player=1;
						}
					}
				}
		
		}
	}
}